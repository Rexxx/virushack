﻿namespace API.Models
{
    public enum ActionType
    {
        PulsePressure,
        Temperature,
        Liquid,
        Defecation,
        SkinDamage,
        Sleep,
        Pain,
        Custom
    }
}