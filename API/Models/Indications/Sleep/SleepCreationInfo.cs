﻿using System;

namespace API.Models.Indications.Sleep
{
    public class SleepCreationInfo : IndicationCreationInfo
    {
        public Guid UserId { get; }
        public DateTime StartAt { get; }
        public DateTime EndAt { get; }
        public bool WokeUpAtNight { get; }
        public bool IsBadSleep { get; }
        public bool IsLongSleep { get; }

        public SleepCreationInfo(Guid userId, DateTime startAt, DateTime endAt, bool? wokeUpAtNight, bool? isBadSleep, bool? isLongSleep, 
            DateTime date, string custom1, string custom2, string custom3, string custom4, string custom5) : base(date, custom1, custom2, custom3, custom4, custom5)
        {
            UserId = userId;
            StartAt = startAt;
            EndAt = endAt;
            WokeUpAtNight = wokeUpAtNight ?? throw new ArgumentNullException(nameof(wokeUpAtNight));
            IsBadSleep = isBadSleep ?? throw new ArgumentNullException(nameof(isBadSleep));
            IsLongSleep = isLongSleep ?? throw new ArgumentNullException(nameof(isLongSleep));
        }
    }
}