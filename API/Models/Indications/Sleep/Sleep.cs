﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace API.Models.Indications.Sleep
{
    public class Sleep : Indication
    {
        [BsonElement("StartAt")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime StartAt { get; set; }
        
        [BsonElement("EndAt")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime EndAt { get; set; }
        
        [BsonElement("WokeUpAtNight")]
        [BsonRepresentation(BsonType.Boolean)]
        public bool WokeUpAtNight { get; set; }
        
        [BsonElement("IsBadSleep")]
        [BsonRepresentation(BsonType.Boolean)]
        public bool IsBadSleep { get; set; }
        
        [BsonElement("IsLongSleep")]
        [BsonRepresentation(BsonType.Boolean)]
        public bool IsLongSleep { get; set; }
    }
}