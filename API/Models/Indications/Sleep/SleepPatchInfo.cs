﻿using System;

namespace API.Models.Indications.Sleep
{
    public class SleepPatchInfo : IndicationPatchInfo
    {
        public Guid Id { get; }
        public DateTime? StartAt { get; }
        public DateTime? EndAt { get; }
        public bool? WokeUpAtNight { get; }
        public bool? IsBadSleep { get; }
        public bool? IsLongSleep { get; }
        
        public SleepPatchInfo(Guid id, DateTime? startAt, DateTime? endAt, bool? wokeUpAtNight, bool? isBadSleep, bool? isLongSleep, 
            string custom1, string custom2, string custom3, string custom4, string custom5) : base(custom1, custom2, custom3, custom4, custom5)
        {
            Id = id;
            StartAt = startAt;
            EndAt = endAt;
            WokeUpAtNight = wokeUpAtNight;
            IsBadSleep = isBadSleep;
            IsLongSleep = isLongSleep;
        }
    }
}