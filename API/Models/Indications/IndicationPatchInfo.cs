﻿namespace API.Models.Indications
{
    public class IndicationPatchInfo
    {
        public string Custom1 { get; }
        public string Custom2 { get; }
        public string Custom3 { get; }
        public string Custom4 { get; }
        public string Custom5 { get; }

        public IndicationPatchInfo(string custom1, string custom2, string custom3, string custom4, string custom5)
        {
            Custom1 = custom1;
            Custom2 = custom2;
            Custom3 = custom3;
            Custom4 = custom4;
            Custom5 = custom5;
        }
    }
}