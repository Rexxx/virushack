﻿using System;

namespace API.Models.Indications.PulsePressure
{
    public class PulsePressurePatchInfo : IndicationPatchInfo
    {
        public Guid Id { get; }
        public int? Pulse { get; }
        public int? High { get; }
        public int? Low { get; }

        public PulsePressurePatchInfo(Guid id, int? pulse, int? high, int? low, 
            string custom1, string custom2, string custom3, string custom4, string custom5) : base(custom1, custom2, custom3, custom4, custom5)
        {
            Id = id;
            Pulse = pulse;
            High = high;
            Low = low;
        }
    }
}