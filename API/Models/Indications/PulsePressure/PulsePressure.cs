﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace API.Models.Indications.PulsePressure
{
    [BsonDiscriminator("PulsePressure")]
    public class PulsePressure : Indication
    {
        [BsonElement("Pulse")]
        [BsonRepresentation(BsonType.Int32)]
        public int Pulse { get; set; }
        
        [BsonElement("High")]
        [BsonRepresentation(BsonType.Int32)]
        public int High { get; set; }
        
        [BsonElement("Low")]
        [BsonRepresentation(BsonType.Int32)]
        public int Low { get; set; }
    }
}