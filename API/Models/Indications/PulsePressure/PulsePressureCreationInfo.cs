﻿using System;

namespace API.Models.Indications.PulsePressure
{
    public class PulsePressureCreationInfo : IndicationCreationInfo
    {
        public Guid UserId { get; }
        public int Pulse { get; }
        public int High { get; }
        public int Low { get; }

        public PulsePressureCreationInfo(Guid userId, int? pulse, int? high, int? low, 
            DateTime date, string custom1, string custom2, string custom3, string custom4, string custom5) : base(date, custom1, custom2, custom3, custom4, custom5)
        {
            UserId = userId;
            Pulse = pulse ?? throw new ArgumentNullException(nameof(pulse));
            High = high ?? throw new ArgumentNullException(nameof(high));
            Low = low ?? throw new ArgumentNullException(nameof(low));
        }
    }
}