﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace API.Models.Indications
{
    [BsonKnownTypes(typeof(PulsePressure.PulsePressure), typeof(Temperature.Temperature), typeof(Sleep.Sleep))]
    public class Indication
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Id { get; set; }
        
        [BsonRepresentation(BsonType.String)]
        public Guid UserId { get; set; }
        
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime Date { get; set; }
        
        [BsonRepresentation(BsonType.String)]
        public string Type { get; set; }
        
        [BsonRepresentation(BsonType.String)]
        public string Custom1 { get; set; }
        
        [BsonRepresentation(BsonType.String)]
        public string Custom2 { get; set; }
        
        [BsonRepresentation(BsonType.String)]
        public string Custom3 { get; set; }
        
        [BsonRepresentation(BsonType.String)]
        public string Custom4 { get; set; }
        
        [BsonRepresentation(BsonType.String)]
        public string Custom5 { get; set; }
        
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime MadeAt { get; set; }        
    }
}