﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace API.Models.Indications.Pain
{
    public class Pain : Indication
    {
        [BsonElement("Intensity")]
        [BsonRepresentation(BsonType.Int32)]
        public int Intensity { get; set; }
    }
}