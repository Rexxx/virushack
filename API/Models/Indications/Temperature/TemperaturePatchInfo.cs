﻿using System;

namespace API.Models.Indications.Temperature
{
    public class TemperaturePatchInfo : IndicationPatchInfo
    {
        public Guid Id { get; }
        public float? Temperature { get; }
        
        public TemperaturePatchInfo(Guid id, float? temperature, string custom1, string custom2, string custom3, 
            string custom4, string custom5) : base(custom1, custom2, custom3, custom4, custom5)
        {
            Id = id;
            Temperature = temperature;
        }
    }
}