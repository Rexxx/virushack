﻿using System;

namespace API.Models.Indications.Temperature
{
    public class TemperatureCreationInfo : IndicationCreationInfo
    {
        public Guid UserId { get; }
        public float Temperature { get; }

        public TemperatureCreationInfo(Guid userId, float? temperature, 
            DateTime date, string custom1, string custom2, string custom3, string custom4, string custom5) : base(date, custom1, custom2, custom3, custom4, custom5)
        {
            UserId = userId;
            Temperature = temperature ?? throw new ArgumentNullException(nameof(temperature));
        }
    }
}