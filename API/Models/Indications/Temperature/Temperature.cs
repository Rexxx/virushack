﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace API.Models.Indications.Temperature
{
    [BsonDiscriminator("Temperature")]
    public class Temperature : Indication
    {
        [BsonElement("Temperature")]
        [BsonRepresentation(BsonType.Double)]
        public float Temp { get; set; }
    }
}