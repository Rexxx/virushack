﻿using System;

namespace API.Models.Indications.Exceptions
{
    public class IndicationNotFoundException : Exception
    {
        public IndicationNotFoundException(Guid id)
            : base($"Indication '{id}' not found.")
        {
        }
    }
}