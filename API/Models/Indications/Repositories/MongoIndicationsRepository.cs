﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using API.Models.Indications.Exceptions;
using MongoDB.Driver;

namespace API.Models.Indications.Repositories
{
    public class MongoIndicationsRepository : IIndicationsRepository
    {
        private readonly IMongoCollection<Indication> indications;

        public MongoIndicationsRepository()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("VirushackDb");
            indications = database.GetCollection<Indication>("Indication");
        }

        public Task<Indication> SaveAsync(Indication indication, CancellationToken cancellationToken)
        {
            if (indication == null)
            {
                throw new ArgumentNullException(nameof(indication));
            }

            cancellationToken.ThrowIfCancellationRequested();
            indications.InsertOne(indication, cancellationToken: cancellationToken);
            return Task.FromResult(indication);
        }

        public Task<Indication> GetAsync(Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var indication = indications.Find(item => item.Id.Equals(id)).FirstOrDefault();

            if (indication == null)
            {
                throw new IndicationNotFoundException(id);
            }

            return Task.FromResult(indication);
        }

        public Task<IReadOnlyList<Indication>> GetAllByIds(IReadOnlyList<Guid> itemIds, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var set = new HashSet<Guid>(itemIds);
            var result = indications.Find(item => set.Contains(item.Id));
                var a = result.ToEnumerable();
                var b = a.ToArray();
            return Task.FromResult<IReadOnlyList<Indication>>(b);
        }

        public Task<Indication> UpdateAsync(Indication updatedIndication, CancellationToken cancellationToken)
        {
            if (updatedIndication == null)
            {
                throw new ArgumentNullException(nameof(updatedIndication));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var indication = indications.Find(item => item.Id == updatedIndication.Id).FirstOrDefault();

            if (indication == null)
            {
                throw new IndicationNotFoundException(updatedIndication.Id);
            }

            indications.ReplaceOne(item => item.Id.Equals(updatedIndication.Id), updatedIndication);
            return Task.FromResult(updatedIndication);
        }

        public Task DeleteAsync(Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var deleteResult = indications.DeleteOne(item => item.Id == id);
            
            if (deleteResult.DeletedCount == 0)
            {
                throw new IndicationNotFoundException(id);
            }

            return Task.CompletedTask;
        }
    }
}