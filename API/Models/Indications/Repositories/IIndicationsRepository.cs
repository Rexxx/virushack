﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace API.Models.Indications.Repositories
{
    public interface IIndicationsRepository
    {
        Task<Indication> SaveAsync(Indication indication, CancellationToken cancellationToken);
        Task<Indication> GetAsync(Guid id, CancellationToken cancellationToken);
        Task<IReadOnlyList<Indication>> GetAllByIds(IReadOnlyList<Guid> itemIds, CancellationToken cancellationToken);
        Task<Indication> UpdateAsync(Indication updatedIndication, CancellationToken cancellationToken);
        Task DeleteAsync(Guid id, CancellationToken cancellationToken);
    }
}