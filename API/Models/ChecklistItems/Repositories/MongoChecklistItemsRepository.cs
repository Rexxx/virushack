﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using API.ModelConverters;
using API.Models.ChecklistItems.Exceptions;
using MongoDB.Driver;

namespace API.Models.ChecklistItems.Repositories
{
    public class MongoChecklistItemsRepository : IChecklistItemsRepository
    {
        private readonly IMongoCollection<ChecklistItem> checklistItems;

        public MongoChecklistItemsRepository()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("VirushackDb");
            checklistItems = database.GetCollection<ChecklistItem>("ChecklistItems");
        }

        public Task<ChecklistItem> CreateAsync(ChecklistItemCreationInfo creationInfo, CancellationToken cancellationToken)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();

            var newId = Guid.NewGuid();
            var newChecklistItem = new ChecklistItem
            {
                Id = newId,
                Type = creationInfo.Type.ToString(),
                Description = creationInfo.Description,
                Time = creationInfo.Time,
                Completed = false
            };

            checklistItems.InsertOne(newChecklistItem, cancellationToken: cancellationToken);
            return Task.FromResult(newChecklistItem);
        }

        public Task<ChecklistItem> GetAsync(Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var checklist = checklistItems.Find(list => list.Id.Equals(id)).FirstOrDefault();

            if (checklist == null)
            {
                throw new ChecklistItemNotFoundException(id);
            }

            return Task.FromResult(checklist);
        }

        public Task<IReadOnlyList<ChecklistItem>> GetAllByIds(IReadOnlyList<Guid> itemIds, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var set = new HashSet<Guid>(itemIds);
            var result = checklistItems.Find(item => set.Contains(item.Id))
                .ToEnumerable()
                .ToArray();
            return Task.FromResult<IReadOnlyList<ChecklistItem>>(result);
        }

        public Task<ChecklistItem> PatchAsync(ChecklistItemPatchInfo patchInfo, CancellationToken cancellationToken)
        {
            if (patchInfo == null)
            {
                throw new ArgumentNullException(nameof(patchInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var checklistItem = checklistItems.Find(item => item.Id == patchInfo.Id).FirstOrDefault();

            if (checklistItem == null)
            {
                throw new ChecklistItemNotFoundException(patchInfo.Id);
            }

            var updated = false;

            if (patchInfo.Type != null)
            {
                checklistItem.Type = patchInfo.Type;
                updated = true;
            }
            
            if (patchInfo.Description != null)
            {
                checklistItem.Description = patchInfo.Description;
                updated = true;
            }
            
            if (patchInfo.Time != null)
            {
                var dateTime = DateTimeConverter.CombineDateAndTime(checklistItem.Time, patchInfo.Time.Value);
                checklistItem.Time = dateTime;
                updated = true;
            }

            if (patchInfo.Completed != null)
            {
                checklistItem.Completed = patchInfo.Completed.Value;
                updated = true;
            }

            if (updated)
            {
                checklistItems.ReplaceOne(item => item.Id.Equals(patchInfo.Id), checklistItem);
            }

            return Task.FromResult(checklistItem);
        }

        public Task DeleteAsync(Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var deleteResult = checklistItems.DeleteOne(item => item.Id == id);
            
            if (deleteResult.DeletedCount == 0)
            {
                throw new ChecklistItemNotFoundException(id);
            }

            return Task.CompletedTask;
        }
    }
}