﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace API.Models.ChecklistItems.Repositories
{
    public interface IChecklistItemsRepository
    {
        Task<ChecklistItem> CreateAsync(ChecklistItemCreationInfo creationInfo, CancellationToken cancellationToken);
        Task<ChecklistItem> GetAsync(Guid id, CancellationToken cancellationToken);
        Task<IReadOnlyList<ChecklistItem>> GetAllByIds(IReadOnlyList<Guid> itemIds, CancellationToken cancellationToken);
        Task<ChecklistItem> PatchAsync(ChecklistItemPatchInfo patchInfo, CancellationToken cancellationToken);
        Task DeleteAsync(Guid id, CancellationToken cancellationToken);
    }
}