﻿using System;

namespace API.Models.ChecklistItems
{
    public class ChecklistItemCreationInfo
    {
        public ActionType Type { get; }
        public string Description { get; }
        public DateTime Time { get; }

        public ChecklistItemCreationInfo(ActionType type, string description, DateTime time)
        {
            Type = type;
            Description = description ?? string.Empty;
            Time = time;
        }
    }
}