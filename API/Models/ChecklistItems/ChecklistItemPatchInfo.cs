﻿using System;

namespace API.Models.ChecklistItems
{
    public class ChecklistItemPatchInfo
    {
        public Guid Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public DateTime? Time { get; set; }
        public bool? Completed { get; set; }
    }
}