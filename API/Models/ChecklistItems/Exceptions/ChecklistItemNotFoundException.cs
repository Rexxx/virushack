﻿using System;

namespace API.Models.ChecklistItems.Exceptions
{
    public class ChecklistItemNotFoundException : Exception
    {
        public ChecklistItemNotFoundException(Guid id)
            : base($"Checklist item with id '{id}' not found.")
        {
        }
    }
}