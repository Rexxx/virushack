﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace API.Models.ChecklistItems
{
    public class ChecklistItem
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Id { get; set; }
        
        [BsonElement("Type")]
        [BsonRepresentation(BsonType.String)]
        public string Type { get; set; }
        
        [BsonElement("Description")]
        [BsonRepresentation(BsonType.String)]
        public string Description { get; set; }
        
        [BsonElement("Time")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime Time { get; set; }
        
        [BsonElement("Completed")]
        [BsonRepresentation(BsonType.Boolean)]
        public bool Completed { get; set; }
    }
}