﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace API.Models.Checklists.Repositories
{
    public interface IChecklistsRepository
    {
        Task<Checklist> CreateAsync(ChecklistCreationInfo creationInfo, CancellationToken cancellationToken);
        Task<Checklist> GetAsync(Guid id, CancellationToken cancellationToken);
        Task<Checklist> GetByDateAsync(Guid userId, DateTime date, CancellationToken cancellationToken);
        Task AddItemAsync(Guid userId, DateTime date, Guid itemId, CancellationToken cancellationToken);
        Task DeleteItemAsync(Guid userId, DateTime date, Guid itemId, CancellationToken cancellationToken);
    }
}