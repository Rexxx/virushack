﻿using System;
using System.Threading;
using System.Threading.Tasks;
using API.Models.Checklists.Exceptions;
using MongoDB.Driver;

namespace API.Models.Checklists.Repositories
{
    public class MongoChecklistsRepository : IChecklistsRepository
    {
        private readonly IMongoCollection<Checklist> checklists;

        public MongoChecklistsRepository()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("VirushackDb");
            checklists = database.GetCollection<Checklist>("Checklist");
        }

        public Task<Checklist> CreateAsync(ChecklistCreationInfo creationInfo, CancellationToken cancellationToken)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();

            if (FindByUserAndDate(creationInfo.UserId, creationInfo.Date) != null)
            {
                throw new ChecklistDuplicationException(creationInfo.Date);
            }
            
            var newId = Guid.NewGuid();
            var newChecklist = new Checklist
            {
                Id = newId,
                UserId = creationInfo.UserId,
                Items = new Guid[0], 
                Date = creationInfo.Date
            };

            checklists.InsertOne(newChecklist, cancellationToken: cancellationToken);
            return Task.FromResult(newChecklist);
        }

        public Task<Checklist> GetAsync(Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var checklist = checklists.Find(list => list.Id.Equals(id)).FirstOrDefault();

            if (checklist == null)
            {
                throw new ChecklistNotFoundException(id);
            }

            return Task.FromResult(checklist);
        }

        public Task<Checklist> GetByDateAsync(Guid userId, DateTime date, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var checklist = FindByUserAndDate(userId, date);
            
            if (checklist == null)
            {
                throw new ChecklistNotFoundException(date);
            }
            
            return Task.FromResult(checklist);
        }

        public async Task AddItemAsync(Guid userId, DateTime date, Guid itemId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var checklist = FindByUserAndDate(userId, date);

            if (checklist == null)
            {
                throw new ChecklistNotFoundException(date);
            }

            var builder = Builders<Checklist>.Filter;
            var filter = builder.Eq("UserId", userId) & builder.Eq("Date", date);
            var update = Builders<Checklist>.Update.Push("Items", itemId);
            await checklists.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);
        }

        public async Task DeleteItemAsync(Guid userId, DateTime date, Guid itemId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var checklist = FindByUserAndDate(userId, date);

            if (checklist == null)
            {
                throw new ChecklistNotFoundException(date);
            }
            
            var builder = Builders<Checklist>.Filter;
            var filter = builder.Eq("UserId", userId) & builder.Eq("Date", date);
            var update = Builders<Checklist>.Update.Pull("Items", itemId);
            await checklists.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);
        }

        private Checklist FindByUserAndDate(Guid userId, DateTime date)
        {
            return checklists.Find(item => 
                item.UserId.Equals(userId) && item.Date.Equals(date)).FirstOrDefault();
        }
    }
}