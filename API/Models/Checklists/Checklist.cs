﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace API.Models.Checklists
{
    public class Checklist
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Id { get; set; }
        
        [BsonElement("UserId")]
        [BsonRepresentation(BsonType.String)]
        public Guid UserId { get; set; }
        
        [BsonElement("Items")]
        public IReadOnlyList<Guid> Items { get; set; }
        
        [BsonElement("Date")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime Date { get; set; }
    }
}