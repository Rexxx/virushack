﻿using System;

namespace API.Models.Checklists.Exceptions
{
    public class ChecklistDuplicationException : Exception
    {
        public ChecklistDuplicationException(DateTime date)
            : base($"Checklist with date '{date:yyyy-MM-dd}' already exists.")
        {
        
        }
    }
}