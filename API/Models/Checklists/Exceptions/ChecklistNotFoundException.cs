﻿using System;

namespace API.Models.Checklists.Exceptions
{
    public class ChecklistNotFoundException : Exception
    {
        public ChecklistNotFoundException(Guid id)
            : base($"Checklist '{id}' not found.")
        {
        }

        public ChecklistNotFoundException(DateTime date)
            : base($"Checklist with date '{date:yyyy-MM-dd}' not found.")
        {
            
        }
    }
}