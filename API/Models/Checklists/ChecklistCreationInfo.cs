﻿using System;

namespace API.Models.Checklists
{
    public class ChecklistCreationInfo
    {
        public Guid UserId { get; }
        public DateTime Date { get; }

        public ChecklistCreationInfo(Guid userId, DateTime date)
        {
            UserId = userId;
            Date = date;
        }
    }
}