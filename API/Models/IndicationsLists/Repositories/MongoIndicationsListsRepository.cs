﻿using System;
using System.Threading;
using System.Threading.Tasks;
using API.Models.IndicationsLists.Exceptions;
using MongoDB.Driver;

namespace API.Models.IndicationsLists.Repositories
{
    public class MongoIndicationsListsRepository : IIndicationsListsRepository
    {
        private readonly IMongoCollection<IndicationsList> indicationsLists;

        public MongoIndicationsListsRepository()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("VirushackDb");
            indicationsLists = database.GetCollection<IndicationsList>("IndicationsList");
        }

        public Task<IndicationsList> CreateAsync(IndicationsListCreationInfo creationInfo, CancellationToken cancellationToken)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();

            if (FindByUserAndDate(creationInfo.UserId, creationInfo.Date) != null)
            {
                throw new IndicationsListDuplicationException(creationInfo.Date);
            }
            
            var newId = Guid.NewGuid();
            var newIndicationsList = new IndicationsList
            {
                Id = newId,
                UserId = creationInfo.UserId,
                Items = new Guid[0], 
                Date = creationInfo.Date
            };

            indicationsLists.InsertOne(newIndicationsList, cancellationToken: cancellationToken);
            return Task.FromResult(newIndicationsList);
        }

        public Task<IndicationsList> GetAsync(Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var indicationsList = indicationsLists.Find(list => list.Id.Equals(id)).FirstOrDefault();

            if (indicationsList == null)
            {
                throw new IndicationsListNotFoundException(id);
            }

            return Task.FromResult(indicationsList);
        }

        public Task<IndicationsList> GetByDateAsync(Guid userId, DateTime date, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var indicationsList = FindByUserAndDate(userId, date);
            
            if (indicationsList == null)
            {
                throw new IndicationsListNotFoundException(date);
            }
            
            return Task.FromResult(indicationsList);
        }

        public async Task AddItemAsync(Guid userId, DateTime date, Guid itemId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var indicationsList = FindByUserAndDate(userId, date);

            if (indicationsList == null)
            {
                throw new IndicationsListNotFoundException(date);
            }

            var builder = Builders<IndicationsList>.Filter;
            var filter = builder.Eq("UserId", userId) & builder.Eq("Date", date);
            var update = Builders<IndicationsList>.Update.Push("Items", itemId);
            await indicationsLists.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);
        }

        public async Task DeleteItemAsync(Guid userId, DateTime date, Guid itemId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var indicationsList = FindByUserAndDate(userId, date);

            if (indicationsList == null)
            {
                throw new IndicationsListNotFoundException(date);
            }

            var builder = Builders<IndicationsList>.Filter;
            var filter = builder.Eq("UserId", userId) & builder.Eq("Date", date);
            var update = Builders<IndicationsList>.Update.Pull("Items", itemId);
            await indicationsLists.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);
        }

        private IndicationsList FindByUserAndDate(Guid userId, DateTime date)
        {
            return indicationsLists.Find(item => 
                item.UserId.Equals(userId) && item.Date.Equals(date)).FirstOrDefault();
        }
    }
}