﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace API.Models.IndicationsLists.Repositories
{
    public interface IIndicationsListsRepository
    {
        Task<IndicationsList> CreateAsync(IndicationsListCreationInfo creationInfo, CancellationToken cancellationToken);
        Task<IndicationsList> GetAsync(Guid id, CancellationToken cancellationToken);
        Task<IndicationsList> GetByDateAsync(Guid userId, DateTime date, CancellationToken cancellationToken);
        Task AddItemAsync(Guid userId, DateTime date, Guid itemId, CancellationToken cancellationToken);
        Task DeleteItemAsync(Guid userId, DateTime date, Guid itemId, CancellationToken cancellationToken);
    }
}