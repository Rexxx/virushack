﻿using System;

namespace API.Models.IndicationsLists.Exceptions
{
    public class IndicationsListNotFoundException : Exception
    {
        public IndicationsListNotFoundException(Guid id)
            : base($"IndicationsList '{id}' not found.")
        {
        }

        public IndicationsListNotFoundException(DateTime date)
            : base($"IndicationsList with date '{date:yyyy-MM-dd}' not found.")
        {
        }
    }
}