﻿using System;

namespace API.Models.IndicationsLists.Exceptions
{
    public class IndicationsListDuplicationException : Exception
    {
        public IndicationsListDuplicationException(DateTime date)
            : base($"IndicationsList with date '{date:yyyy-MM-dd}' already exists.")
        {
        
        }
    }
}