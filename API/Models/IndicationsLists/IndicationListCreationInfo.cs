﻿using System;

namespace API.Models.IndicationsLists
{
    public class IndicationsListCreationInfo
    {
        public Guid UserId { get; }
        public DateTime Date { get; }

        public IndicationsListCreationInfo(Guid userId, DateTime date)
        {
            UserId = userId;
            Date = date;
        }
    }
}