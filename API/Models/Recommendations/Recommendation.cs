﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace API.Models.Recommendations
{
    public class Recommendation
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Id { get; set; }
        
        [BsonElement("Name")]
        [BsonRepresentation(BsonType.String)]
        public string Name { get; set; }
        
        [BsonElement("URL")]
        [BsonRepresentation(BsonType.String)]
        public string URL { get; set; }
        
        [BsonElement("Type")]
        [BsonRepresentation(BsonType.String)]
        public string Type { get; set; }
    }
}