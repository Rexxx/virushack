﻿using System;
using System.Threading;
using System.Threading.Tasks;
using API.ClientModels.Recommendations;

namespace API.Models.Recommendations.Repositories
{
    public interface IRecommendationRepository
    {
        Task<Recommendation> CreateAsync(RecommendationCreationInfo creationInfo, CancellationToken cancellationToken);
        Task<Recommendation> GetAsync(Guid id, CancellationToken cancellationToken);
        Task<Recommendation[]> GetAllByTypeAsync(RecommendationType type, CancellationToken cancellationToken);
        Task DeleteAsync(Guid id, CancellationToken cancellationToken);
    }
}