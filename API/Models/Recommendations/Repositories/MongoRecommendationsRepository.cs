﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using API.ClientModels.Recommendations;
using API.Models.Recommendations.Exceptions;
using MongoDB.Driver;

namespace API.Models.Recommendations.Repositories
{
    public class MongoRecommendationsRepository : IRecommendationRepository
    {
        private readonly IMongoCollection<Recommendation> recommendations;

        public MongoRecommendationsRepository()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("VirushackDb");
            recommendations = database.GetCollection<Recommendation>("Recommendation");
        }

        Task<Recommendation> IRecommendationRepository.CreateAsync(RecommendationCreationInfo creationInfo, CancellationToken cancellationToken)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();

            var newRecommendation = new Recommendation
            {
                Id = Guid.NewGuid(),
                Name = creationInfo.Name,
                Type = creationInfo.Type.Value.ToString(),
                URL = creationInfo.URL
            };
            
            recommendations.InsertOne(newRecommendation, cancellationToken: cancellationToken);
            return Task.FromResult(newRecommendation);
        }
        
        Task<Recommendation> IRecommendationRepository.GetAsync(Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var recommendation = recommendations.Find(list => list.Id.Equals(id)).FirstOrDefault();

            if (recommendation == null)
            {
                throw new RecommendationNotFoundException(id);
            }

            return Task.FromResult(recommendation);
        }

        public Task<Recommendation[]> GetAllByTypeAsync(RecommendationType type, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var typeStr = type.ToString();
            var result = recommendations.Find(item =>
                item.Type.Equals(typeStr)).ToEnumerable().ToArray();

            return Task.FromResult(result);
        }

        public Task DeleteAsync(Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var deleteResult = recommendations.DeleteOne(item => item.Id == id);
            
            if (deleteResult.DeletedCount == 0)
            {
                throw new RecommendationNotFoundException(id);
            }

            return Task.CompletedTask;
        }
    }
}