﻿using System;

namespace API.Models.Recommendations.Exceptions
{
    public class RecommendationNotFoundException : Exception
    {
        public RecommendationNotFoundException(Guid id)
            : base($"Recommendation '{id}' not found.")
        {
        }
    }
}