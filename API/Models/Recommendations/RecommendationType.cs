﻿namespace API.Models.Recommendations
{
    public enum RecommendationType
    {
        Bedsore,
        FallingRisk,
        Tracheostomy
    }
}