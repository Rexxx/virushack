using AspNetCore.Identity.Mongo.Model;

namespace API.Models.Roles
{
    public class Role : MongoRole
    {
        public Role() : base() { }

        public Role(string name) : base(name)
        {
        }
    }
}