﻿using System.ComponentModel.DataAnnotations;

namespace API.ClientModels
{
    public class DateInfo
    {
        [Required(ErrorMessage = "Field 'date' in format yyyy-MM-dd is required")]
        public string Date { get; set; }
    }
}