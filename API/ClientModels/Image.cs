namespace API.ClientModels
{
    public class Image
    {
        public string Url { get; set; }
    }
}