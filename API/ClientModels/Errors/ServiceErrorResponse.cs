using System.Net;
using ClientModels.Errors;

namespace API.ClientModels.Errors
{
    public class ServiceErrorResponse
    {
        public HttpStatusCode StatusCode { get; set; }
        public ServiceError Error { get; set; }
    }
}