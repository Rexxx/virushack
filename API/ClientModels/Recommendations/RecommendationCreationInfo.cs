﻿using System.ComponentModel.DataAnnotations;
using API.Models.Recommendations;

namespace API.ClientModels.Recommendations
{
    public class RecommendationCreationInfo
    {
        [Required(ErrorMessage = "Field 'name' is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Field 'url' is required")]
        public string URL { get; set; }
        [Required(ErrorMessage = "Field 'type' is required")]
        public RecommendationType? Type { get; set; }
    }
}