﻿namespace API.ClientModels.Indications.Sleep
{
    public class SleepPatchInfo : IndicationPatchInfo
    {
        public string StartAt { get; set; }
        public string EndAt { get; set; }
        public bool? WokeUpAtNight { get; set; }
        public bool? IsBadSleep { get; set; }
        public bool? IsLongSleep { get; set; }
    }
}