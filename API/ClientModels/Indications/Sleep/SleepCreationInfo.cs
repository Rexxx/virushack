﻿using System.ComponentModel.DataAnnotations;

namespace API.ClientModels.Indications.Sleep
{
    public class SleepCreationInfo : IndicationCreationInfo
    {
        [Required(ErrorMessage = "Field 'startAt' is required in format hh:mm")]
        public string StartAt { get; set; }
        [Required(ErrorMessage = "Field 'startAt' is required in format hh:mm")]
        public string EndAt { get; set; }
        [Required(ErrorMessage = "Field 'wokeUpAtNight' is required")]
        public bool? WokeUpAtNight { get; set; }
        [Required(ErrorMessage = "Field 'isBadSleep' is required")]
        public bool? IsBadSleep { get; set; }
        [Required(ErrorMessage = "Field 'isLongSleep' is required")]
        public bool? IsLongSleep { get; set; }
    }
}