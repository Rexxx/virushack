﻿namespace API.ClientModels.Indications
{
    public class IndicationPatchInfo
    {
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Custom4 { get; set; }
        public string Custom5 { get; set; }
    }
}