﻿using System.ComponentModel.DataAnnotations;

namespace API.ClientModels.Indications.Temperature
{
    public class TemperaturePatchInfo : IndicationPatchInfo
    {
        [Range(20,47)]
        public float? Temperature { get; set; }
    }
}