﻿using System.ComponentModel.DataAnnotations;

namespace API.ClientModels.Indications.Temperature
{
    public class TemperatureCreationInfo : IndicationCreationInfo
    {
        [Required(ErrorMessage = "Field 'temperature' is required")]
        [Range(20, 47, ErrorMessage = "'Temperature' value must be in rage 20 and 47")]
        public float? Temperature { get; set; }
    }
}