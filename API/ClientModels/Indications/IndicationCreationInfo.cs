﻿using System.ComponentModel.DataAnnotations;

namespace API.ClientModels.Indications
{
    public class IndicationCreationInfo
    {
        [Required]
        public string Date { get; set; }
        // Не задавайтесь вопросом, зачем нужны эти поля снизу
        // Вынужденная прихоть фронта из-за бесконечного потока фич от аналитика :)
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Custom4 { get; set; }
        public string Custom5 { get; set; }
    }
}