﻿using System.ComponentModel.DataAnnotations;

namespace API.ClientModels.Indications.PulsePressure
{
    public class PulsePressurePatchInfo : IndicationPatchInfo
    {
        [Range(20, 600, ErrorMessage = "'Pulse' value must be in range 20 and 600")]
        public int? Pulse { get; set; }
        [Range(20, 300, ErrorMessage = "'High' value must be in range 20 and 300")]
        public int? High { get; set; }
        [Range(20, 300, ErrorMessage = "'Low' value must be in range 20 and 300")]
        public int? Low { get; set; }
    }
}