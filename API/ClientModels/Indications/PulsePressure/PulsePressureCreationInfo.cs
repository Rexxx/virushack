﻿using System.ComponentModel.DataAnnotations;

namespace API.ClientModels.Indications.PulsePressure
{
    public class PulsePressureCreationInfo : IndicationCreationInfo
    {
        [Required(ErrorMessage = "Field 'pulse' is required")]
        [Range(20, 600, ErrorMessage = "'Pulse' value must be in range 20 and 600")]
        public int? Pulse { get; set; }
        [Required(ErrorMessage = "Field 'high' is required")]
        [Range(20, 300, ErrorMessage = "'High' value must be in range 20 and 300")]
        public int? High { get; set; }
        [Required(ErrorMessage = "Field 'low' is required")]
        [Range(20, 300, ErrorMessage = "'Low' value must be in range 20 and 300")]
        public int? Low { get; set; }
    }
}