﻿using System;
using System.Collections.Generic;
using API.Models.Indications;

namespace API.ClientModels.IndicationsLists
{
    public class IndicationsList
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public IReadOnlyList<Indication> Items { get; set; }
        public DateTime Date { get; set; }
    }
}