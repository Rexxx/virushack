﻿using System.ComponentModel.DataAnnotations;
using API.Models;

namespace API.ClientModels.ChecklistItems
{
    public class ChecklistItemCreationInfo
    {
        [Required(ErrorMessage = "Field 'type' is required")]
        public ActionType? Type { get; set; }
        public string Description { get; set; }
        [Required(ErrorMessage = "Field 'date' is required")]
        public string Date { get; set; }
        public string Time { get; set; }
    }
}