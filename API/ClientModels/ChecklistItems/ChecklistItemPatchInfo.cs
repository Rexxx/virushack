﻿using API.Models;

namespace API.ClientModels.ChecklistItems
{
    public class ChecklistItemPatchInfo
    {
        public string Type { get; set; }
        public string Description { get; set; }
        public string Time { get; set; }
        public bool? Completed { get; set; }
    }
}