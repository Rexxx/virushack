using System.ComponentModel.DataAnnotations;

namespace API.ClientModels.UserIdentity
{
    public class UserLogin
    {
        [Required(ErrorMessage = "Field 'username' is required")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Field 'password' is required")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Field 'rememberMe' is required")]
        public bool RememberMe { get; set; }
    }
}