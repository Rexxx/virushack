﻿using System;
using System.Collections.Generic;
using API.Models.ChecklistItems;

namespace API.ClientModels.Checklists
{
    public class Checklist
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public IReadOnlyList<ChecklistItem> Items { get; set; }
        public DateTime Date { get; set; }
    }
}