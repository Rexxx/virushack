﻿using System;
using System.Threading;
using System.Threading.Tasks;
using API.ModelConverters.Indications;
using API.Models.Indications;
using API.Models.Indications.Exceptions;
using API.Models.Indications.Repositories;
using API.Models.Indications.Sleep;
using API.Models.IndicationsLists;
using API.Models.IndicationsLists.Exceptions;
using API.Models.IndicationsLists.Repositories;
using API.Services;
using Microsoft.AspNetCore.Mvc;
using SleepCreationInfo = API.ClientModels.Indications.Sleep.SleepCreationInfo;

namespace API.Controllers
{
    [Route("api/sleep")]
    public class SleepController : ControllerBase
    {
        private readonly IIndicationsRepository repository;
        private readonly IIndicationsListsRepository indicationsListsRepository;

        public SleepController(IIndicationsRepository repository, IIndicationsListsRepository indicationsListsRepository)
        {
            this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
            this.indicationsListsRepository = indicationsListsRepository ??
                                              throw new ArgumentNullException(nameof(indicationsListsRepository));
        }
        
        /// <summary>
        /// Creates sleep item
        /// </summary>
        /// <param name="creationInfo">Sleep creation info</param>
        /// <param name="cancellationToken"></param>
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreateSleepAsync([FromBody]SleepCreationInfo creationInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(SleepCreationInfo), message);
                return BadRequest(error);
            }

            // todo достаём юзера из авторизации
            // var userName = HttpContext.User.Identity.Name;
            // var user = await userManager.FindByNameAsync(userName);
            var userId = Guid.Empty;
            Models.Indications.Sleep.SleepCreationInfo modelCreationInfo;

            try
            {
                modelCreationInfo = SleepConverter.Convert(userId, creationInfo);
            }
            catch (Exception ex)
            {
                var error = ErrorResponsesService.BadRequest(nameof(SleepCreationInfo), ex.Message);
                return BadRequest(error);
            }
            
            var indication = IndicationCreationService.Create(modelCreationInfo);
            var sleep = await repository.SaveAsync(indication, cancellationToken).ConfigureAwait(false);
            
            try
            {
                await indicationsListsRepository.AddItemAsync(userId, modelCreationInfo.Date, sleep.Id, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (IndicationsListNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(IndicationsList), ex.Message);
                return BadRequest(error);
            }

            return CreatedAtRoute("GetSleepRoute", new { id = sleep.Id }, sleep);
        }
        
        /// <summary>
        /// Returns a sleep by id
        /// </summary>
        /// <param name="id">Sleep id</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("{id}", Name = "GetSleepRoute")]
        public async Task<IActionResult> GetSleepAsync([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            Indication sleep;

            try
            {
                sleep = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (IndicationNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Sleep), ex.Message);
                return NotFound(error);
            }

            return Ok(sleep);
        }
        
        /// <summary>
        /// Updates sleep item
        /// </summary>
        /// <param name="id">Sleep id</param>
        /// <param name="patchInfo">Sleep patch info</param>
        /// <param name="cancellationToken"></param>
        [HttpPatch]
        [Route("{id}")]
        public async Task<IActionResult> PatchSleepAsync([FromRoute]Guid id, 
            [FromBody] ClientModels.Indications.Sleep.SleepPatchInfo patchInfo, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(SleepPatchInfo), message);
                return BadRequest(error);
            }

            SleepPatchInfo modelPatchInfo;
            Sleep sleep;

            
            try {
                modelPatchInfo = SleepConverter.Convert(id, patchInfo);
            }
            catch (Exception ex)
            {
                var error = ErrorResponsesService.BadRequest(nameof(SleepPatchInfo), ex.Message);
                return BadRequest(error);
            }
            
            try
            {
                sleep = (Sleep) await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (IndicationNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Sleep), ex.Message);
                return NotFound(error);
            }
            
            IndicationPatchService.Patch(sleep, modelPatchInfo);
            var sleepItem = await repository.UpdateAsync(sleep, cancellationToken).ConfigureAwait(false);
            return Ok(sleepItem);
        }
        
        /// <summary>
        /// Removes sleep item
        /// </summary>
        /// <param name="id">Sleep id</param>
        /// <param name="cancellationToken"></param>
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteSleepAsync([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            try
            {
                var indication = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
                await repository.DeleteAsync(id, cancellationToken).ConfigureAwait(false);
                
                // todo достаём юзера из авторизации
                // var userName = HttpContext.User.Identity.Name;
                // var user = await userManager.FindByNameAsync(userName);
                var userId = Guid.Empty;
                await indicationsListsRepository.DeleteItemAsync(userId, indication.Date, id, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (IndicationNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Sleep), ex.Message);
                return NotFound(error);
            }
            catch (IndicationsListNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(IndicationsList), ex.Message);
                return BadRequest(error);
            }

            return NoContent();
        }
    }
}