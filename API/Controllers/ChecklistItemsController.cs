﻿using System;
using System.Threading;
using System.Threading.Tasks;
using API.ModelConverters;
using API.Models;
using API.Models.ChecklistItems;
using API.Models.ChecklistItems.Exceptions;
using API.Models.ChecklistItems.Repositories;
using API.Models.Checklists;
using API.Models.Checklists.Exceptions;
using API.Models.Checklists.Repositories;
using API.Services;
using Microsoft.AspNetCore.Mvc;
using ChecklistItemCreationInfo = API.ClientModels.ChecklistItems.ChecklistItemCreationInfo;

namespace API.Controllers
{
    [Route("api/checklist-items")]
    public class ChecklistItemsController : ControllerBase
    {
        private readonly IChecklistItemsRepository repository;
        private readonly IChecklistsRepository checklistsRepository;

        public ChecklistItemsController(IChecklistItemsRepository repository, IChecklistsRepository checklistsRepository)
        {
            this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
            this.checklistsRepository =
                checklistsRepository ?? throw new ArgumentNullException(nameof(checklistsRepository));
        }
        
        /// <summary>
        /// Creates checklist item
        /// </summary>
        /// <param name="creationInfo">ChecklistItem creation info</param>
        /// <param name="cancellationToken"></param>
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreateChecklistItemAsync([FromBody]ChecklistItemCreationInfo creationInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(ChecklistItemCreationInfo), message);
                return BadRequest(error);
            }

            Models.ChecklistItems.ChecklistItemCreationInfo modelCreationInfo;

            try
            {
                modelCreationInfo = ChecklistItemConverter.Convert(creationInfo);
            }
            catch (Exception ex)
            {
                var error = ErrorResponsesService.BadRequest(nameof(ChecklistItemCreationInfo), ex.Message);
                return BadRequest(error);
            }
            
            // todo достаём юзера из авторизации
            // var userName = HttpContext.User.Identity.Name;
            // var user = await userManager.FindByNameAsync(userName);
            var userId = Guid.Empty;
            var checklistItem = await repository.CreateAsync(modelCreationInfo, cancellationToken).ConfigureAwait(false);
            
            try
            {
                var date = DateTimeConverter.ConvertDateTimeFromClientToModel(creationInfo.Date, null);
                await checklistsRepository.AddItemAsync(userId, date, checklistItem.Id, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (ChecklistNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError("Checklist", ex.Message);
                return BadRequest(error);
            }
            
            return CreatedAtRoute("GetChecklistItemRoute", new { id = checklistItem.Id }, checklistItem);
        }
        
        /// <summary>
        /// Returns a checklistItem by id
        /// </summary>
        /// <param name="id">ChecklistItem id</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("{id}", Name = "GetChecklistItemRoute")]
        public async Task<IActionResult> GetChecklistAsync([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ChecklistItem checklistItem;

            try
            {
                checklistItem = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (ChecklistItemNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(ChecklistItem), ex.Message);
                return NotFound(error);
            }

            return Ok(checklistItem);
        }
        
        /// <summary>
        /// Updates checklist item
        /// </summary>
        /// <param name="id">ChecklistItem id</param>
        /// <param name="patchInfo">ChecklistItem patch info</param>
        /// <param name="cancellationToken"></param>
        [HttpPatch]
        [Route("{id}")]
        public async Task<IActionResult> PatchChecklistItemAsync([FromRoute]Guid id, 
            [FromBody] ClientModels.ChecklistItems.ChecklistItemPatchInfo patchInfo, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (patchInfo.Type != null)
            {
                if (!Enum.TryParse<ActionType>(patchInfo.Type, true, out var result))
                {
                    var error = ErrorResponsesService.BadRequest(nameof(ChecklistItemPatchInfo),
                        $"Value '{patchInfo.Type}' is not enum of {nameof(ActionType)}");
                    return BadRequest(error);
                }
                else
                {
                    patchInfo.Type = result.ToString();
                }
            }
            
            ChecklistItemPatchInfo modelPatchInfo;

            try
            {
                modelPatchInfo = ChecklistItemConverter.Convert(id, patchInfo);
            }
            catch (Exception ex)
            {
                var error = ErrorResponsesService.BadRequest(nameof(ChecklistItemPatchInfo), ex.Message);
                return BadRequest(error);
            }
            
            ChecklistItem checklistItem;
            
            try
            {
                checklistItem = await repository.PatchAsync(modelPatchInfo, cancellationToken).ConfigureAwait(false);
            }
            catch (ChecklistItemNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(ChecklistItem), ex.Message);
                return NotFound(error);
            }

            return Ok(checklistItem);
        }
        
        /// <summary>
        /// Removes checklist item
        /// </summary>
        /// <param name="id">ChecklistItem id</param>
        /// <param name="cancellationToken"></param>
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteChecklistItemAsync([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ChecklistItem item;

            try
            {
                item = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (ChecklistItemNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(ChecklistItem), ex.Message);
                return NotFound(error);
            }

            try
            {
                await repository.DeleteAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (ChecklistItemNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(ChecklistItem), ex.Message);
                return NotFound(error);
            }
            
            try
            {
                // todo достаём юзера из авторизации
                // var userName = HttpContext.User.Identity.Name;
                // var user = await userManager.FindByNameAsync(userName);
                var userId = Guid.Empty;
                var date = DateTimeConverter.ResetTimeToZero(item.Time);
                await checklistsRepository.DeleteItemAsync(userId, date, id, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (ChecklistNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Checklist), ex.Message);
                return BadRequest(error);
            }

            return NoContent();
        }
    }
}