﻿using System;
using System.Threading;
using System.Threading.Tasks;
using API.ClientModels.Recommendations;
using API.Models.Recommendations;
using API.Models.Recommendations.Exceptions;
using API.Models.Recommendations.Repositories;
using API.Services;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/recommendations")]
    public class RecommendationsController : ControllerBase
    {
        private readonly IRecommendationRepository repository;

        public RecommendationsController(IRecommendationRepository repository)
        {
            this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }
        
        /// <summary>
        /// Creates recommendation
        /// </summary>
        /// <param name="creationInfo">Recommendation creation info</param>
        /// <param name="cancellationToken"></param>
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreateRecommendationAsync([FromBody]RecommendationCreationInfo creationInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(RecommendationCreationInfo), message);
                return BadRequest(error);
            }

            var recommendation = await repository.CreateAsync(creationInfo, cancellationToken).ConfigureAwait(false);
            return CreatedAtRoute("GetRecommendationRoute", new { id = recommendation.Id }, recommendation);
        }
        
        /// <summary>
        /// Returns a recommendation by id
        /// </summary>
        /// <param name="id">Recommendation id</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("{id}", Name = "GetRecommendationRoute")]
        public async Task<IActionResult> GetRecommendationByIdAsync([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            Recommendation recommendation;

            try
            {
                recommendation = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (RecommendationNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Recommendation), ex.Message);
                return NotFound(error);
            }

            return Ok(recommendation);
        }

        /// <summary>
        /// Returns recommendations by type
        /// </summary>
        /// <param name="type">Recommendation type</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetRecommendationsAsync([FromQuery]RecommendationType type,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var recommendations = await repository.GetAllByTypeAsync(type, cancellationToken).ConfigureAwait(false);
            return Ok(recommendations);
        }
        
        /// <summary>
        /// Removes recommendation
        /// </summary>
        /// <param name="id">Recommendation id</param>
        /// <param name="cancellationToken"></param>
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteRecommendationAsync([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            try
            {
                await repository.DeleteAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (RecommendationNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Recommendation), ex.Message);
                return NotFound(error);
            }
            
            return NoContent();
        }
    }
}