﻿using System;
using System.Threading;
using System.Threading.Tasks;
using API.ClientModels;
using API.ModelConverters;
using API.Models.ChecklistItems.Repositories;
using API.Models.Checklists;
using API.Models.Checklists.Exceptions;
using API.Models.Checklists.Repositories;
using API.Services;
using Microsoft.AspNetCore.Mvc;
using DateTimeConverter = API.ModelConverters.DateTimeConverter;

namespace API.Controllers
{
    [Route("api/checklists")]
    public class ChecklistsController : ControllerBase
    {
        private readonly IChecklistsRepository repository;
        private readonly IChecklistItemsRepository itemsRepository;

        public ChecklistsController(IChecklistsRepository repository, IChecklistItemsRepository itemsRepository)
        {
            this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
            this.itemsRepository = itemsRepository ?? throw new ArgumentNullException(nameof(itemsRepository));
        }
        
        /// <summary>
        /// Creates checklist
        /// </summary>
        /// <param name="date">Checklist date</param>
        /// <param name="cancellationToken"></param>
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreateChecklistAsync([FromBody]DateInfo date,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(DateInfo), message);
                return BadRequest(error);
            }
            
            DateTime modelDate;

            try
            {
                modelDate = DateTimeConverter.ConvertDateTimeFromClientToModel(date.Date, null);
            }
            catch (Exception ex)
            {
                var error = ErrorResponsesService.BadRequest(nameof(DateInfo), ex.Message);
                return BadRequest(error);
            }

            // todo достаём юзера из авторизации
            // var userName = HttpContext.User.Identity.Name;
            // var user = await userManager.FindByNameAsync(userName);
            var userId = Guid.Empty;
            var modelCreationInfo = new ChecklistCreationInfo(userId, modelDate);
            Checklist checklist;

            try
            {
                checklist =
                    await repository.CreateAsync(modelCreationInfo, cancellationToken).ConfigureAwait(false);
            }
            catch (ChecklistDuplicationException ex)
            {
                var error = ErrorResponsesService.DuplicationError(nameof(Checklist), ex.Message);
                return BadRequest(error);
            }

            var items = await itemsRepository.GetAllByIds(checklist.Items, cancellationToken).ConfigureAwait(false);
            var clientChecklist = ChecklistConverter.Convert(checklist, items);
            return CreatedAtRoute("GetChecklistRoute", new { id = checklist.Id }, clientChecklist);
        }
        
        /// <summary>
        /// Returns a checklist by id
        /// </summary>
        /// <param name="id">Checklist id</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("{id}", Name = "GetChecklistRoute")]
        public async Task<IActionResult> GetChecklistAsync([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            Checklist checklist;

            try
            {
                checklist = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (ChecklistNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Checklist), ex.Message);
                return NotFound(error);
            }

            var items = await itemsRepository.GetAllByIds(checklist.Items, cancellationToken).ConfigureAwait(false);
            var clientChecklist = ChecklistConverter.Convert(checklist, items);
            return Ok(clientChecklist);
        }
        
        /// <summary>
        /// Returns a checklist by date
        /// </summary>
        /// <param name="dateInfo">Checklist date</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetChecklistByDateAsync([FromQuery]DateInfo dateInfo, 
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(DateInfo), message);
                return BadRequest(error);
            }
            
            DateTime modelDate;

            try
            {
                modelDate = DateTimeConverter.ConvertDateTimeFromClientToModel(dateInfo.Date, null);
            }
            catch (Exception ex)
            {
                var error = ErrorResponsesService.BadRequest(nameof(DateInfo), ex.Message);
                return BadRequest(error);
            }

            // todo достаём юзера из авторизации
            // var userName = HttpContext.User.Identity.Name;
            // var user = await userManager.FindByNameAsync(userName);
            var userId = Guid.Empty;
            Checklist modelChecklist;

            try
            {
                modelChecklist = await repository.GetByDateAsync(userId, modelDate, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (ChecklistNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Checklist), ex.Message);
                return NotFound(error);
            }

            var items = await itemsRepository.GetAllByIds(modelChecklist.Items, cancellationToken).ConfigureAwait(false);
            var clientChecklist = ChecklistConverter.Convert(modelChecklist, items);
            return Ok(clientChecklist);
        }
    }
}