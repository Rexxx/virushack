﻿using System;
using System.Threading;
using System.Threading.Tasks;
using API.ClientModels;
using API.ModelConverters;
using API.Models.Indications.Repositories;
using API.Models.IndicationsLists;
using API.Models.IndicationsLists.Exceptions;
using API.Models.IndicationsLists.Repositories;
using API.Services;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/indications-lists")]
    public class IndicationsListsController : ControllerBase
    {
        private readonly IIndicationsListsRepository repository;
        private readonly IIndicationsRepository indicationsRepository;

        public IndicationsListsController(IIndicationsListsRepository repository, IIndicationsRepository indicationsRepository)
        {
            this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
            this.indicationsRepository = indicationsRepository ?? throw new ArgumentNullException(nameof(indicationsRepository));
        }
        
        /// <summary>
        /// Creates indicationsList
        /// </summary>
        /// <param name="date">IndicationsList date</param>
        /// <param name="cancellationToken"></param>
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreateIndicationsListAsync([FromBody]DateInfo date,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(DateInfo), message);
                return BadRequest(error);
            }
            
            DateTime modelDate;

            try
            {
                modelDate = DateTimeConverter.ConvertDateTimeFromClientToModel(date.Date, null);
            }
            catch (Exception ex)
            {
                var error = ErrorResponsesService.BadRequest(nameof(DateInfo), ex.Message);
                return BadRequest(error);
            }

            // todo достаём юзера из авторизации
            // var userName = HttpContext.User.Identity.Name;
            // var user = await userManager.FindByNameAsync(userName);
            var userId = Guid.Empty;
            var modelCreationInfo = new IndicationsListCreationInfo(userId, modelDate);
            IndicationsList indicationsList;

            try
            {
                indicationsList =
                    await repository.CreateAsync(modelCreationInfo, cancellationToken).ConfigureAwait(false);
            }
            catch (IndicationsListDuplicationException ex)
            {
                var error = ErrorResponsesService.DuplicationError(nameof(IndicationsList), ex.Message);
                return BadRequest(error);
            }

            var items = await indicationsRepository.GetAllByIds(indicationsList.Items, cancellationToken).ConfigureAwait(false);
            var clientIndicationsList = IndicationsListConverter.Convert(indicationsList, items);
            return CreatedAtRoute("GetIndicationsListRoute", new { id = indicationsList.Id }, clientIndicationsList);
        }
        
        /// <summary>
        /// Returns an indicationsList by id
        /// </summary>
        /// <param name="id">IndicationsList id</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("{id}", Name = "GetIndicationsListRoute")]
        public async Task<IActionResult> GetIndicationsListAsync([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            IndicationsList indicationsList;

            try
            {
                indicationsList = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (IndicationsListNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(IndicationsList), ex.Message);
                return NotFound(error);
            }

            var items = await indicationsRepository.GetAllByIds(indicationsList.Items, cancellationToken).ConfigureAwait(false);
            var clientIndicationsList = IndicationsListConverter.Convert(indicationsList, items);
            return Ok(clientIndicationsList);
        }
        
        /// <summary>
        /// Returns an indicationsList by date
        /// </summary>
        /// <param name="dateInfo">IndicationsList date</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetIndicationsListByDateAsync([FromQuery]DateInfo dateInfo, 
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(DateInfo), message);
                return BadRequest(error);
            }
            
            DateTime modelDate;

            try
            {
                modelDate = DateTimeConverter.ConvertDateTimeFromClientToModel(dateInfo.Date, null);
            }
            catch (Exception ex)
            {
                var error = ErrorResponsesService.BadRequest(nameof(DateInfo), ex.Message);
                return BadRequest(error);
            }

            // todo достаём юзера из авторизации
            // var userName = HttpContext.User.Identity.Name;
            // var user = await userManager.FindByNameAsync(userName);
            var userId = Guid.Empty;
            IndicationsList modelIndicationsList;

            try
            {
                modelIndicationsList = await repository.GetByDateAsync(userId, modelDate, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (IndicationsListNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(IndicationsList), ex.Message);
                return NotFound(error);
            }

            var items = await indicationsRepository.GetAllByIds(modelIndicationsList.Items, cancellationToken).ConfigureAwait(false);
            var clientIndicationsList = IndicationsListConverter.Convert(modelIndicationsList, items);
            return Ok(clientIndicationsList);
        }
    }
}