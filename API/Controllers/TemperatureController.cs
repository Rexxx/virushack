﻿using System;
using System.Threading;
using System.Threading.Tasks;
using API.ModelConverters;
using API.ModelConverters.Indications;
using API.Models.Indications;
using API.Models.Indications.Exceptions;
using API.Models.Indications.Repositories;
using API.Models.Indications.Temperature;
using API.Models.IndicationsLists;
using API.Models.IndicationsLists.Exceptions;
using API.Models.IndicationsLists.Repositories;
using API.Services;
using Microsoft.AspNetCore.Mvc;
using TemperatureCreationInfo = API.ClientModels.Indications.Temperature.TemperatureCreationInfo;

namespace API.Controllers
{
    [Route("api/temperature")]
    public class TemperatureController : ControllerBase
    {
        private readonly IIndicationsRepository repository;
        private readonly IIndicationsListsRepository indicationsListsRepository;

        public TemperatureController(IIndicationsRepository repository, IIndicationsListsRepository indicationsListsRepository)
        {
            this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
            this.indicationsListsRepository = indicationsListsRepository ??
                                              throw new ArgumentNullException(nameof(indicationsListsRepository));
        }
        
        /// <summary>
        /// Creates temperature item
        /// </summary>
        /// <param name="creationInfo">Temperature creation info</param>
        /// <param name="cancellationToken"></param>
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreateTemperatureAsync([FromBody]TemperatureCreationInfo creationInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(TemperatureCreationInfo), message);
                return BadRequest(error);
            }

            // todo достаём юзера из авторизации
            // var userName = HttpContext.User.Identity.Name;
            // var user = await userManager.FindByNameAsync(userName);
            var userId = Guid.Empty;
            Models.Indications.Temperature.TemperatureCreationInfo modelCreationInfo;

            try
            {
                modelCreationInfo = TemperatureConverter.Convert(userId, creationInfo);
            }
            catch (Exception ex)
            {
                var error = ErrorResponsesService.BadRequest(nameof(TemperatureCreationInfo), ex.Message);
                return BadRequest(error);
            }
            
            var indication = IndicationCreationService.Create(modelCreationInfo);
            var temperature = await repository.SaveAsync(indication, cancellationToken).ConfigureAwait(false);
            
            try
            {
                await indicationsListsRepository.AddItemAsync(userId, modelCreationInfo.Date, temperature.Id, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (IndicationsListNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(IndicationsList), ex.Message);
                return BadRequest(error);
            }

            return CreatedAtRoute("GetTemperatureRoute", new { id = temperature.Id }, temperature);
        }
        
        /// <summary>
        /// Returns a temperature by id
        /// </summary>
        /// <param name="id">Temperature id</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("{id}", Name = "GetTemperatureRoute")]
        public async Task<IActionResult> GetTemperatureAsync([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            Indication temperature;

            try
            {
                temperature = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (IndicationNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Temperature), ex.Message);
                return NotFound(error);
            }

            return Ok(temperature);
        }
        
        /// <summary>
        /// Updates temperature item
        /// </summary>
        /// <param name="id">Temperature id</param>
        /// <param name="patchInfo">Temperature patch info</param>
        /// <param name="cancellationToken"></param>
        [HttpPatch]
        [Route("{id}")]
        public async Task<IActionResult> PatchTemperatureAsync([FromRoute]Guid id, 
            [FromBody] ClientModels.Indications.Temperature.TemperaturePatchInfo patchInfo, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(TemperatureCreationInfo), message);
                return BadRequest(error);
            }

            var modelPatchInfo = TemperatureConverter.Convert(id, patchInfo);
            Temperature temperature;
            
            try
            {
                temperature = (Temperature) await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (IndicationNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Temperature), ex.Message);
                return NotFound(error);
            }
            
            IndicationPatchService.Patch(temperature, modelPatchInfo);
            var temperatureItem = await repository.UpdateAsync(temperature, cancellationToken).ConfigureAwait(false);
            return Ok(temperatureItem);
        }
        
        /// <summary>
        /// Removes temperature item
        /// </summary>
        /// <param name="id">Temperature id</param>
        /// <param name="cancellationToken"></param>
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteTemperatureAsync([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            try
            {
                var indication = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
                await repository.DeleteAsync(id, cancellationToken).ConfigureAwait(false);
                
                // todo достаём юзера из авторизации
                // var userName = HttpContext.User.Identity.Name;
                // var user = await userManager.FindByNameAsync(userName);
                var userId = Guid.Empty;
                await indicationsListsRepository.DeleteItemAsync(userId, indication.Date, id, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (IndicationNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Temperature), ex.Message);
                return NotFound(error);
            }
            catch (IndicationsListNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(IndicationsList), ex.Message);
                return BadRequest(error);
            }

            return NoContent();
        }
    }
}