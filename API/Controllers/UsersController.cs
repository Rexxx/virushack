using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using API.ClientModels.Users;
using API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using User = API.Models.Users.User;
using UserConverter = API.ModelConverters.UserConverter;
using UserCreationInfo = API.Models.Users.UserCreationInfo;

namespace API.Controllers
{
    [Route("api/users")]
    public class UsersController : ControllerBase
    {
        private readonly UserManager<User> userManager;
        private const string PasswordValidationMessage = "Password and confirmed password don't match.";

        public UsersController(UserManager<User> userManager)
        {
            this.userManager = userManager ?? throw new ArgumentException(nameof(userManager));
        }

        /// <summary>
        /// Creates user
        /// </summary>
        /// <param name="clientUserCreationInfo">User creation info</param>
        /// <param name="cancellationToken"></param>
        [HttpPost]
        [AllowAnonymous]
        [Route("")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> CreateUserAsync([FromBody] ClientModels.Users.UserCreationInfo clientUserCreationInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(UserCreationInfo), message);
                return BadRequest(error);
            }

            if (!clientUserCreationInfo.Password.Equals(clientUserCreationInfo.ConfirmPassword))
            {
                var error = ErrorResponsesService.ValidationError(nameof(clientUserCreationInfo.ConfirmPassword), 
                    PasswordValidationMessage);
                return BadRequest(error);
            }

            UserCreationInfo modelCreationInfo;
            
            try
            {
                modelCreationInfo = UserConverter.Convert(clientUserCreationInfo);
            }
            catch (ArgumentNullException ex)
            {
                var error = ErrorResponsesService.InvalidCredentialsError(ex.Message);
                return BadRequest(error);
            }
            
            var user = await userManager.FindByNameAsync(modelCreationInfo.UserName);

            if (user != null)
            {
                var error = ErrorResponsesService.DuplicationError(nameof(User), $"User with name '{clientUserCreationInfo.UserName}' already exists.");
                return BadRequest(error);
            }

            var dateTime = DateTime.UtcNow;
            var modelUser = new User
            {
                UserName = modelCreationInfo.UserName,
                Name = modelCreationInfo.Name,
                Email = modelCreationInfo.Email,
                PhoneNumber = modelCreationInfo.PhoneNumber,
                RegisteredAt = dateTime,
                LastUpdateAt = dateTime
            };

            var result = await userManager.CreateAsync(modelUser, modelCreationInfo.Password);

            if (!result.Succeeded)
            {
                var error = ErrorResponsesService.ValidationError(nameof(clientUserCreationInfo), 
                    result.Errors.First().Description);
                return BadRequest(error);
            }

            await userManager.AddToRoleAsync(modelUser, "user");
            var clientUser = UserConverter.Convert(modelUser);
            return CreatedAtRoute("GetUserRoute", new { username = clientUser.UserName }, clientUser);
        }

        /// <summary>
        /// Gets users list
        /// </summary>
        /// <param name="clientSearchInfo">Search filters</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("")]
        [Authorize(Roles = "admin")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> GetAllUsersAsync([FromQuery]UserSearchInfo clientSearchInfo, 
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var users = await Task.Run(() => userManager.Users, cancellationToken);

            var modelSearchInfo = UserConverter.Convert(clientSearchInfo ?? new UserSearchInfo());

            if (modelSearchInfo.Offset != null)
            {
                users = users.Skip(modelSearchInfo.Offset.Value);
            }

            if (modelSearchInfo.Limit != null)
            {
                users = users.Take(modelSearchInfo.Limit.Value);
            }

            users = users.OrderByDescending(item => item.LastUpdateAt);

            var clientUsers = users.Select(user => UserConverter.Convert(user));
            return Ok(clientUsers);
        }

        /// <summary>
        /// Gets user info by user login
        /// </summary>
        /// <param name="userName">User login</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("{userName}", Name = "GetUserRoute")]
        [Authorize]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> GetUserAsync([FromRoute]string userName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!HasAccessToUser(userName))
            {
                return Forbid();
            }

            var user = await userManager.FindByNameAsync(userName);

            if (user == null)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(User), $"User with name '{userName}' not found.");
                return BadRequest(error);
            }

            var clientUser = UserConverter.Convert(user);
            return Ok(clientUser);
        }

        /// <summary>
        /// Gets user info by cookie
        /// </summary>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("info")]
        [Authorize]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetUserInfoAsync(CancellationToken cancellationToken)
        {
            if (HttpContext.User?.Identity != null && HttpContext.User.Identity.IsAuthenticated)
            {
                var user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);

                if (user != null)
                {
                    var clientUser = UserConverter.Convert(user);
                    return Ok(clientUser);
                }
            }

            var error = ErrorResponsesService.UnauthorizedError(nameof(User));
            return NotFound(error);
        }

        /// <summary>
        /// Patches user info
        /// </summary>
        /// <param name="userName">User login</param>
        /// <param name="clientPatchInfo">Patch info</param>
        /// <param name="cancellationToken"></param>
        [HttpPatch]
        [Route("{userName}")]
        [Authorize]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> PatchUserAsync([FromRoute] string userName, [FromBody] UserPatchInfo clientPatchInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(clientPatchInfo), message);
                return BadRequest(error);
            }

            if (!HasAccessToUser(userName))
            {
                return Forbid();
            }

            var modelPatchInfo = UserConverter.Convert(userName, clientPatchInfo);
            var user = await userManager.FindByNameAsync(userName);

            if (user == null)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(User), $"User with name '{userName}' not found.");
                return NotFound(error);
            }

            var updated = false;

            if (modelPatchInfo.OldPassword != null && modelPatchInfo.Password != null)
            {
                var passwordHasher = HttpContext.RequestServices.
                    GetService(typeof(IPasswordHasher<User>)) as IPasswordHasher<User>;
                //todo password validation doesn't work
//                var oldPasswordHash = passwordHasher.HashPassword(user, modelPatchInfo.OldPassword);
//
//                if (!oldPasswordHash.Equals(user.PasswordHash))
//                {
//                    var error = ErrorResponsesService.ValidationError(nameof(clientPatchInfo.OldPassword), 
//                        "Old password doesn't match with actual.");
//                    return BadRequest(error);
//                }
                
                if (!modelPatchInfo.Password.Equals(modelPatchInfo.ConfirmPassword))
                {
                    var error = ErrorResponsesService.ValidationError(nameof(clientPatchInfo.ConfirmPassword), 
                        PasswordValidationMessage);
                    return BadRequest(error);
                }
                
                var passwordValidator = HttpContext.RequestServices.
                    GetService(typeof(IPasswordValidator<User>)) as IPasswordValidator<User>;
                var result = await passwordValidator.ValidateAsync(userManager, user, modelPatchInfo.Password);

                if (result.Succeeded)
                {
                    user.PasswordHash = passwordHasher.HashPassword(user, modelPatchInfo.Password);
                    updated = true;
                }
            }

            if (updated)
            {
                user.LastUpdateAt = DateTime.UtcNow;
                await userManager.UpdateAsync(user);
            }

            var clientUser = UserConverter.Convert(user);
            return Ok(clientUser);
        }

        /// <summary>
        /// Removes user
        /// </summary>
        /// <param name="userName">User login</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("{userName}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        public async Task<ActionResult> DeleteUserAsync([FromRoute]string userName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!HasAccessToUser(userName))
            {
                return Forbid();
            }

            var user = await userManager.FindByNameAsync(userName);
            
            if (user == null)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(User), $"User with name '{userName}' not found.");
                return NotFound(error);
            }

            await userManager.DeleteAsync(user);
            return NoContent();
        }
        
        private bool HasAccessToUser(string userName)
        {
            return HttpContext.User.IsInRole("admin") ||
                   string.Compare(HttpContext.User.Identity.Name, userName.ToLower(), StringComparison.Ordinal) == 0;
        }
    }
}