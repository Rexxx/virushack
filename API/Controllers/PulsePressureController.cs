﻿using System;
using System.Threading;
using System.Threading.Tasks;
using API.ModelConverters.Indications;
using API.Models.Indications;
using API.Models.Indications.Exceptions;
using API.Models.Indications.PulsePressure;
using API.Models.Indications.Repositories;
using API.Models.IndicationsLists;
using API.Models.IndicationsLists.Exceptions;
using API.Models.IndicationsLists.Repositories;
using API.Services;
using Microsoft.AspNetCore.Mvc;
using PulsePressureCreationInfo = API.ClientModels.Indications.PulsePressure.PulsePressureCreationInfo;


namespace API.Controllers
{
    [Route("api/pulse-pressure")]
    public class PulsePressureController : ControllerBase
    {
        private readonly IIndicationsRepository repository;
        private readonly IIndicationsListsRepository indicationsListsRepository;

        public PulsePressureController(IIndicationsRepository repository, IIndicationsListsRepository indicationsListsRepository)
        {
            this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
            this.indicationsListsRepository = indicationsListsRepository ??
                                              throw new ArgumentNullException(nameof(indicationsListsRepository));
        }
        
        /// <summary>
        /// Creates pulsePressure item
        /// </summary>
        /// <param name="creationInfo">PulsePressure creation info</param>
        /// <param name="cancellationToken"></param>
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreatePulsePressureAsync([FromBody]PulsePressureCreationInfo creationInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(PulsePressureCreationInfo), message);
                return BadRequest(error);
            }

            // todo достаём юзера из авторизации
            // var userName = HttpContext.User.Identity.Name;
            // var user = await userManager.FindByNameAsync(userName);
            var userId = Guid.Empty;
            Models.Indications.PulsePressure.PulsePressureCreationInfo modelCreationInfo;

            try
            {
                modelCreationInfo = PulsePressureConverter.Convert(userId, creationInfo);
            }
            catch (Exception ex)
            {
                var error = ErrorResponsesService.BadRequest(nameof(PulsePressureCreationInfo.Date), ex.Message);
                return BadRequest(error);
            }
            
            var indication = IndicationCreationService.Create(modelCreationInfo);
            var pulsePressure = await repository.SaveAsync(indication, cancellationToken).ConfigureAwait(false);
            
            try
            {
                await indicationsListsRepository.AddItemAsync(userId, modelCreationInfo.Date, pulsePressure.Id, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (IndicationsListNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(IndicationsList), ex.Message);
                return BadRequest(error);
            }

            return CreatedAtRoute("GetPulsePressureRoute", new { id = pulsePressure.Id }, pulsePressure);
        }
        
        /// <summary>
        /// Returns a pulsePressure by id
        /// </summary>
        /// <param name="id">PulsePressure id</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("{id}", Name = "GetPulsePressureRoute")]
        public async Task<IActionResult> GetPulsePressureAsync([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            Indication pulsePressure;

            try
            {
                pulsePressure = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (IndicationNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(PulsePressure), ex.Message);
                return NotFound(error);
            }

            return Ok(pulsePressure);
        }
        
        /// <summary>
        /// Updates pulsePressure item
        /// </summary>
        /// <param name="id">PulsePressure id</param>
        /// <param name="patchInfo">PulsePressure patch info</param>
        /// <param name="cancellationToken"></param>
        [HttpPatch]
        [Route("{id}")]
        public async Task<IActionResult> PatchPulsePressureAsync([FromRoute]Guid id, 
            [FromBody] ClientModels.Indications.PulsePressure.PulsePressurePatchInfo patchInfo, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(PulsePressurePatchInfo), message);
                return BadRequest(error);
            }

            var modelPatchInfo = PulsePressureConverter.Convert(id, patchInfo);
            PulsePressure pulsePressure;
            
            try
            {
                pulsePressure = (PulsePressure) await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (IndicationNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(PulsePressure), ex.Message);
                return NotFound(error);
            }
            
            IndicationPatchService.Patch(pulsePressure, modelPatchInfo);
            var pulsePressureItem = await repository.UpdateAsync(pulsePressure, cancellationToken).ConfigureAwait(false);
            return Ok(pulsePressureItem);
        }
        
        /// <summary>
        /// Removes pulsePressure item
        /// </summary>
        /// <param name="id">PulsePressure id</param>
        /// <param name="cancellationToken"></param>
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeletePulsePressureAsync([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            try
            {
                var indication = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
                await repository.DeleteAsync(id, cancellationToken).ConfigureAwait(false);
                
                // todo достаём юзера из авторизации
                // var userName = HttpContext.User.Identity.Name;
                // var user = await userManager.FindByNameAsync(userName);
                var userId = Guid.Empty;
                await indicationsListsRepository.DeleteItemAsync(userId, indication.Date, id, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (IndicationNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(PulsePressure), ex.Message);
                return NotFound(error);
            }
            catch (IndicationsListNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(IndicationsList), ex.Message);
                return NotFound(error);
            }

            return NoContent();
        }
    }
}