﻿using System;
using API.ModelConverters;
using API.Models.Indications;
using API.Models.Indications.PulsePressure;
using API.Models.Indications.Sleep;
using API.Models.Indications.Temperature;

namespace API.Services
{
    public static class IndicationPatchService
    {
        public static void Patch(PulsePressure pulsePressure, PulsePressurePatchInfo patchInfo)
        {
            if (pulsePressure == null)
            {
                throw new ArgumentNullException(nameof(pulsePressure));
            }
            
            if (patchInfo == null)
            {
                throw new ArgumentNullException(nameof(patchInfo));
            }

            if (patchInfo.Pulse != null)
            {
                pulsePressure.Pulse = patchInfo.Pulse.Value;
            }
            
            if (patchInfo.High != null)
            {
                pulsePressure.High = patchInfo.High.Value;
            }
            
            if (patchInfo.Low != null)
            {
                pulsePressure.Low = patchInfo.Low.Value;
            }

            PatchCustomFields(pulsePressure, patchInfo);
        }
        
        public static void Patch(Temperature temperature, TemperaturePatchInfo patchInfo)
        {
            if (temperature == null)
            {
                throw new ArgumentNullException(nameof(temperature));
            }
            
            if (patchInfo == null)
            {
                throw new ArgumentNullException(nameof(patchInfo));
            }

            if (patchInfo.Temperature != null)
            {
                temperature.Temp = patchInfo.Temperature.Value;
            }
            
            PatchCustomFields(temperature, patchInfo);
        }
        
        public static void Patch(Sleep sleep, SleepPatchInfo patchInfo)
        {
            if (sleep == null)
            {
                throw new ArgumentNullException(nameof(sleep));
            }
            
            if (patchInfo == null)
            {
                throw new ArgumentNullException(nameof(patchInfo));
            }

            if (patchInfo.StartAt != null)
            {
                // тут важно брать только время, потому что дата это стаб
                var startAt = DateTimeConverter.CombineDateAndTime(sleep.StartAt, patchInfo.StartAt.Value);
                sleep.StartAt = startAt;
            }
            
            if (patchInfo.EndAt != null)
            {
                // тут важно брать только время, потому что дата это стаб
                var endAt = DateTimeConverter.CombineDateAndTime(sleep.EndAt, patchInfo.EndAt.Value);
                sleep.EndAt = endAt;
            }

            if (sleep.EndAt.CompareTo(sleep.StartAt) < 0)
            {
                sleep.EndAt = sleep.EndAt.AddDays(1);
            }
            
            if (patchInfo.WokeUpAtNight != null)
            {
                sleep.WokeUpAtNight = patchInfo.WokeUpAtNight.Value;
            }
            
            if (patchInfo.IsBadSleep != null)
            {
                sleep.IsBadSleep = patchInfo.IsBadSleep.Value;
            }
            
            if (patchInfo.IsLongSleep != null)
            {
                sleep.IsLongSleep = patchInfo.IsLongSleep.Value;
            }
            
            PatchCustomFields(sleep, patchInfo);
        }

        private static void PatchCustomFields(Indication indication, IndicationPatchInfo patchInfo)
        {
            if (indication == null)
            {
                throw new ArgumentNullException(nameof(indication));
            }
            
            if (patchInfo == null)
            {
                throw new ArgumentNullException(nameof(patchInfo));
            }

            if (patchInfo.Custom1 != null)
            {
                indication.Custom1 = patchInfo.Custom1;
            }
            
            if (patchInfo.Custom2 != null)
            {
                indication.Custom2 = patchInfo.Custom2;
            }
            
            if (patchInfo.Custom3 != null)
            {
                indication.Custom3 = patchInfo.Custom3;
            }
            
            if (patchInfo.Custom4 != null)
            {
                indication.Custom4 = patchInfo.Custom4;
            }
            
            if (patchInfo.Custom5 != null)
            {
                indication.Custom5 = patchInfo.Custom5;
            }
        }
    }
}