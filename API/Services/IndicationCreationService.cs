﻿using System;
using API.Models;
using API.Models.Indications.PulsePressure;
using API.Models.Indications.Sleep;
using API.Models.Indications.Temperature;

namespace API.Services
{
    public static class IndicationCreationService
    {
        public static PulsePressure Create(PulsePressureCreationInfo creationInfo)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }

            return new PulsePressure
            {
                Id = Guid.NewGuid(),
                Pulse = creationInfo.Pulse,
                High = creationInfo.High,
                Low = creationInfo.Low,
                Type = ActionType.PulsePressure.ToString(),
                UserId = creationInfo.UserId,
                Date = creationInfo.Date,
                MadeAt = DateTime.UtcNow,
                Custom1 = creationInfo.Custom1,
                Custom2 = creationInfo.Custom2,
                Custom3 = creationInfo.Custom3,
                Custom4 = creationInfo.Custom4,
                Custom5 = creationInfo.Custom5
            };
        }

        public static Temperature Create(TemperatureCreationInfo creationInfo)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }

            return new Temperature
            {
                Id = Guid.NewGuid(),
                Temp = creationInfo.Temperature,
                Type = ActionType.Temperature.ToString(),
                UserId = creationInfo.UserId,
                Date = creationInfo.Date,
                MadeAt = DateTime.UtcNow,
                Custom1 = creationInfo.Custom1,
                Custom2 = creationInfo.Custom2,
                Custom3 = creationInfo.Custom3,
                Custom4 = creationInfo.Custom4,
                Custom5 = creationInfo.Custom5
            };
        }
        
        public static Sleep Create(SleepCreationInfo creationInfo)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }

            return new Sleep
            {
                Id = Guid.NewGuid(),
                StartAt = creationInfo.StartAt,
                EndAt = creationInfo.EndAt,
                WokeUpAtNight = creationInfo.WokeUpAtNight,
                IsBadSleep = creationInfo.IsBadSleep,
                IsLongSleep = creationInfo.IsLongSleep,
                Type = ActionType.Sleep.ToString(),
                UserId = creationInfo.UserId,
                Date = creationInfo.Date,
                MadeAt = DateTime.UtcNow,
                Custom1 = creationInfo.Custom1,
                Custom2 = creationInfo.Custom2,
                Custom3 = creationInfo.Custom3,
                Custom4 = creationInfo.Custom4,
                Custom5 = creationInfo.Custom5
            };
        }
    }
}