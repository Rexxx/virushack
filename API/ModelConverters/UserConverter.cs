using System;
using API.Models.Users;

namespace API.ModelConverters
{
    public static class UserConverter
    {
        public static ClientModels.Users.User Convert(User modelUser)
        {
            if (modelUser == null)
            {
                throw new ArgumentNullException(nameof(modelUser));
            }

            var clientUser = new ClientModels.Users.User
            {
                Id = modelUser.Id,
                UserName = modelUser.NormalizedUserName,
                Name = modelUser.Name,
                Email = modelUser.Email,
                PhoneNumber = modelUser.PhoneNumber,
                Roles = modelUser.Roles,
                RegisteredAt = modelUser.RegisteredAt,
                LastUpdateAt = modelUser.LastUpdateAt
            };

            return clientUser;
        }
        
        public static UserCreationInfo Convert(ClientModels.Users.UserCreationInfo clientCreationInfo)
        {
            if (clientCreationInfo == null)
            {
                throw new ArgumentNullException(nameof(clientCreationInfo));
            }

            if (string.IsNullOrEmpty(clientCreationInfo.UserName))
            {
                throw new ArgumentNullException(nameof(clientCreationInfo.UserName));
            }
            
            if (string.IsNullOrEmpty(clientCreationInfo.Name))
            {
                throw new ArgumentNullException(nameof(clientCreationInfo.Name));
            }

            if (string.IsNullOrEmpty(clientCreationInfo.Email))
            {
                throw new ArgumentNullException(nameof(clientCreationInfo.Email));
            }

            if (string.IsNullOrEmpty(clientCreationInfo.PhoneNumber))
            {
                throw new ArgumentNullException(nameof(clientCreationInfo.PhoneNumber));
            }

            if (string.IsNullOrEmpty(clientCreationInfo.Password))
            {
                throw new ArgumentNullException(nameof(clientCreationInfo.Password));
            }

            var modelCreationInfo = new UserCreationInfo(
                clientCreationInfo.UserName, 
                clientCreationInfo.Name,
                clientCreationInfo.Email,
                clientCreationInfo.PhoneNumber,
                clientCreationInfo.Password
            );

            return modelCreationInfo;
        }
        
        public static UserPatchInfo Convert(string username, ClientModels.Users.UserPatchInfo clientPatchInfo)
        {
            if (clientPatchInfo == null)
            {
                throw new ArgumentNullException(nameof(clientPatchInfo));
            }

            var modelPatchInfo = new UserPatchInfo(username)
            {
                OldPassword = clientPatchInfo.OldPassword,
                Password = clientPatchInfo.Password,
                ConfirmPassword = clientPatchInfo.ConfirmPassword
            };

            return modelPatchInfo;
        }
        
        public static UserSearchInfo Convert(ClientModels.Users.UserSearchInfo clientSearchInfo)
        {
            if (clientSearchInfo == null)
            {
                throw new ArgumentNullException(nameof(clientSearchInfo));
            }

            var modelSearchInfo = new UserSearchInfo
            {
                Offset = clientSearchInfo.Offset,
                Limit = clientSearchInfo.Limit
            };

            return modelSearchInfo;
        }
    }
}