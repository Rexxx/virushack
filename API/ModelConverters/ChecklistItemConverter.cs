﻿using System;
using Client = API.ClientModels.ChecklistItems;
using Model = API.Models.ChecklistItems;

namespace API.ModelConverters
{
    public static class ChecklistItemConverter
    {
        public static Model.ChecklistItemCreationInfo Convert(Client.ChecklistItemCreationInfo creationInfo)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }

            var time = DateTimeConverter.ConvertDateTimeFromClientToModel(creationInfo.Date, creationInfo.Time);
            return new Model.ChecklistItemCreationInfo(creationInfo.Type.Value, creationInfo.Description, time);
        }

        public static Model.ChecklistItemPatchInfo Convert(Guid id, Client.ChecklistItemPatchInfo patchInfo)
        {
            if (patchInfo == null)
            {
                throw new ArgumentNullException(nameof(patchInfo));
            }
            
            var time = patchInfo.Time == null ? (DateTime?) null : DateTimeConverter.ConvertDateTimeFromClientToModel(null, patchInfo.Time);
            return new Model.ChecklistItemPatchInfo
            {
                Id = id,
                Type = patchInfo.Type,
                Description = patchInfo.Description,
                Time = time,
                Completed = patchInfo.Completed
            };
        }
    }
}