﻿using System;
using System.Collections.Generic;
using API.Models.ChecklistItems;
using Client = API.ClientModels.Checklists;
using Model = API.Models.Checklists;

namespace API.ModelConverters
{
    public static class ChecklistConverter
    {
        public static Client.Checklist Convert(Model.Checklist checklist, IReadOnlyList<ChecklistItem> items)
        {
            if (checklist == null)
            {
                throw new ArgumentNullException(nameof(checklist));
            }

            return new Client.Checklist
            {
                Id = checklist.Id,
                UserId = checklist.UserId,
                Items = items,
                Date = checklist.Date
            };
        }
    }
}