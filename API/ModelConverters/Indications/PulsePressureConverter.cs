﻿using System;
using Client = API.ClientModels.Indications.PulsePressure;
using Model = API.Models.Indications.PulsePressure;

namespace API.ModelConverters.Indications
{
    public static class PulsePressureConverter
    {
        public static Model.PulsePressureCreationInfo Convert(Guid userId, Client.PulsePressureCreationInfo creationInfo)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }

            var date = DateTimeConverter.ConvertDateTimeFromClientToModel(creationInfo.Date, null);
            return new Model.PulsePressureCreationInfo(userId, creationInfo.Pulse, creationInfo.High, creationInfo.Low, 
                date, creationInfo.Custom1, creationInfo.Custom2, creationInfo.Custom3, creationInfo.Custom4, creationInfo.Custom5);
        }

        public static Model.PulsePressurePatchInfo Convert(Guid id, Client.PulsePressurePatchInfo patchInfo)
        {
            if (patchInfo == null)
            {
                throw new ArgumentNullException(nameof(patchInfo));
            }
            
            return new Model.PulsePressurePatchInfo(id, patchInfo.Pulse, patchInfo.High, patchInfo.Low, 
                patchInfo.Custom1, patchInfo.Custom2, patchInfo.Custom3, patchInfo.Custom4, patchInfo.Custom5);
        }
    }
}