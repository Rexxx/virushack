﻿using System;
using Client = API.ClientModels.Indications.Sleep;
using Model = API.Models.Indications.Sleep;

namespace API.ModelConverters.Indications
{
    public static class SleepConverter
    {
        public static Model.SleepCreationInfo Convert(Guid userId, Client.SleepCreationInfo creationInfo)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }

            var date = DateTimeConverter.ConvertDateTimeFromClientToModel(creationInfo.Date, null);
            var startAt = DateTimeConverter.ConvertDateTimeFromClientToModel(creationInfo.Date, creationInfo.StartAt);
            var endAt = DateTimeConverter.ConvertDateTimeFromClientToModel(creationInfo.Date, creationInfo.EndAt);

            if (endAt.CompareTo(startAt) < 0)
            {
                endAt = endAt.AddDays(1);
            }
            
            return new Model.SleepCreationInfo(userId, startAt, endAt, creationInfo.WokeUpAtNight, creationInfo.IsBadSleep, creationInfo.IsLongSleep,
                date, creationInfo.Custom1, creationInfo.Custom2, creationInfo.Custom3, creationInfo.Custom4, creationInfo.Custom5);
        }

        public static Model.SleepPatchInfo Convert(Guid id, Client.SleepPatchInfo patchInfo)
        {
            if (patchInfo == null)
            {
                throw new ArgumentNullException(nameof(patchInfo));
            }

            var startAt = patchInfo.StartAt == null ? (DateTime?) null : 
                DateTimeConverter.ConvertDateTimeFromClientToModel(null, patchInfo.StartAt);
            var endAt = patchInfo.EndAt == null ? (DateTime?) null : 
                DateTimeConverter.ConvertDateTimeFromClientToModel(null, patchInfo.EndAt);
            
            return new Model.SleepPatchInfo(id, startAt, endAt, patchInfo.WokeUpAtNight, patchInfo.IsBadSleep, patchInfo.IsLongSleep,
                patchInfo.Custom1, patchInfo.Custom2, patchInfo.Custom3, patchInfo.Custom4, patchInfo.Custom5);
        }
    }
}