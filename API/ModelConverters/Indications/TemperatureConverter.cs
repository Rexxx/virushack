﻿using System;
using Client = API.ClientModels.Indications.Temperature;
using Model = API.Models.Indications.Temperature;

namespace API.ModelConverters.Indications
{
    public class TemperatureConverter
    {
        public static Model.TemperatureCreationInfo Convert(Guid userId, Client.TemperatureCreationInfo creationInfo)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }

            var date = DateTimeConverter.ConvertDateTimeFromClientToModel(creationInfo.Date, null);
            return new Model.TemperatureCreationInfo(userId, creationInfo.Temperature,
                date, creationInfo.Custom1, creationInfo.Custom2, creationInfo.Custom3, creationInfo.Custom4, creationInfo.Custom5);
        }

        public static Model.TemperaturePatchInfo Convert(Guid id, Client.TemperaturePatchInfo patchInfo)
        {
            if (patchInfo == null)
            {
                throw new ArgumentNullException(nameof(patchInfo));
            }
            
            return new Model.TemperaturePatchInfo(id, patchInfo.Temperature,
                patchInfo.Custom1, patchInfo.Custom2, patchInfo.Custom3, patchInfo.Custom4, patchInfo.Custom5);
        }
    }
}