﻿using System;
using System.Linq;

namespace API.ModelConverters
{
    public static class DateTimeConverter
    {
        private const string ZeroDay = "2000-01-01";
        private const string ZeroTime = "00:00";

        public static DateTime ConvertDateTimeFromClientToModel(string date, string time)
        {
            if (string.IsNullOrEmpty(date))
            {
                date = ZeroDay;
            }

            if (string.IsNullOrEmpty(time))
            {
                time = ZeroTime;
            }
            
            var dateElements = date.Split('-')
                .Select(int.Parse)
                .ToArray();
            var timeElements = time.Split(':')
                .Select(int.Parse)
                .ToArray();
            var modelDateTime = 
                new DateTime(dateElements[0], dateElements[1], dateElements[2], timeElements[0], timeElements[1], 0);
            var normalizedDateTime = DateTime.SpecifyKind(modelDateTime, DateTimeKind.Utc);

            return normalizedDateTime;
        }

        public static string ConvertDateFromModelToClient(DateTime dateTime)
        {
            var clientDate = $"{dateTime.Year}-{dateTime.Month}-{dateTime.Day}";
            return clientDate;
        }

        public static string ConvertTimeFromModelToClient(DateTime dateTime)
        {
            var hourString = dateTime.Hour < 10 ? $"0{dateTime.Hour}" : $"{dateTime.Hour}";
            var minuteString = dateTime.Minute < 10 ? $"0{dateTime.Minute}" : $"{dateTime.Minute}";
            var clientTime = $"{hourString}:{minuteString}";
            return clientTime;
        }

        public static DateTime ResetTimeToZero(DateTime dateTime)
        {
            var date = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 0, 0, 0);
            return DateTime.SpecifyKind(date, DateTimeKind.Utc);
        }

        public static DateTime CombineDateAndTime(DateTime date, DateTime time)
        {
            var dateTime = new DateTime(date.Year, date.Month, date.Day, time.Hour, time.Minute, 0);
            return DateTime.SpecifyKind(dateTime, DateTimeKind.Utc);
        }
    }
}