﻿using System;
using System.Collections.Generic;
using API.Models.Indications;
using API.Models.IndicationsLists;
using Client = API.ClientModels.IndicationsLists;

namespace API.ModelConverters
{
    public static class IndicationsListConverter
    {
        public static Client.IndicationsList Convert(IndicationsList checklist, IReadOnlyList<Indication> items)
        {
            if (checklist == null)
            {
                throw new ArgumentNullException(nameof(checklist));
            }

            return new Client.IndicationsList
            {
                Id = checklist.Id,
                UserId = checklist.UserId,
                Items = items,
                Date = checklist.Date
            };
        }
    }
}